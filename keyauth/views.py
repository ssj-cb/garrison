from django.shortcuts import render
from keyauth.models import *
import string
import secrets
import hashlib

# Create your views here.

# Username
# The client or account the user is associated with
# Scope - the scope of the key. IE, when a function goes to auth a request, it will send it's scope. it must match, case sensitive.
# Role - functions can send the role, ie is the user reading, writing etc.
def create_key(user, account, scope, role):
    # Generate key
    res = AuthKey.objects.filter(user=user, account=account)
    if res.count() == 0: #doesn't already exist
        newkey = AuthKey(user=user, account=account, scope=scope, role=role)
        raw_key = secrets.token_hex(32)
        newkey.salt = secrets.token_hex(32)
        hash = hashlib.sha3_512((newkey.salt + raw_key).encode('utf-8')).hexdigest()
        print("Length of Hash: "+str(len(hash)))
        print("Length of Salt: "+str(len(newkey.salt)))
        newkey.key = hash
        newkey.save()
        return raw_key
    else:
        return None

# user - the key in question
# account, scope, role - the request/supplied variables
def check_user_properties(user, scope, role, account):
        if user.scope.lower() == scope.lower():
            if user.role.lower() == role().lower():
                if user.role.contains("+") == False: # the role is not an 'elevated' role
                    if user.account() == account.lower():
                        return "[Account pass][Scope pass][Role pass]", True
                    else:
                        return "[Account fail][Scope pass][Role pass]", False
                else:
                    return "[Account ignored][Scope pass][Role pass]", True
            else:
                return "[Scope pass][Role fail]", False
        else:
            return "[Role fail]", False


def auth_key(user, account="", scope="", role="", key=""):
    res = AuthKey.objects.filter(user=user)
    if res.count() == 1: #doesn't already exist
        target = res[0]

        decided = False
        result = False
        message = ""
        if target.banned == True:
            message += "[Ban Fail]"
        bmessage, behavior = check_user_behavior(target)
        message += bmessage
        if behavior == False:
            message += "[Behavior Fail]"+bmessage

        key_match =  check_user_key(target, key)
        if key_match == False:
            message+="[Key check failed]"

        message, props = check_user_properties(target, scope, role, account)

        # TODO - create auth history

        return (target.banned and behavior and key_match and props), message
    else:
        return False, "[No user]"

def check_user_key(user, key):
    hash = hashlib.sha3_512((user.salt + key).encode('utf-8')).hexdigest()
    print("Calculated hash: "+hash+"  VS   Actual hash: "+user.key)
    if hash == user.key:
        return True
    return False

# user = the AuthLey obejct
def check_user_behavior(user):
    name = user.user
    hist = AuthHistory.objects.filter(key=user)
    message = ""
    if hist.count() == 0:
        message += "[First login]"
        return message, True
    else:
        return "[TODO]",True

