from django.db import models
# Authenticating Keys - A simple authentication mechanism to be used within another application.
# Exposure of this app to end users should not happen.

# When authing, a function checks the usser auth.
# Scope --- a free text field describing some portion of an application. caller determines it's own scope.
#      If the user and key aren't in the scope, auth denied.
# Account --- A free text field descrbing what account the user is in. A way to group keys together for organizational purposes.
#      A call can use this to restrict authetnications as well
# Role --- A function determiens it's own role. IE if the function is changing data, it should send the request with a 'rw'
#      '<X>-' roles are limited by the 'account', it will force account checking.
#      '<X>+' roles aren't limited by account, it will not care if the account matches.
#      'A' - allows changing of keys.
class AuthKey(models.Model):
    user = models.CharField(max_length=32, default="") # public name

    salt = models.CharField(max_length=64, default="") # the salt

    # private key - Should be SHA512 salted hash
    key = models.CharField(max_length=128, default="")

    # The company/org this key is associated with.
    account = models.CharField(max_length=32, default="") # Account/organization associated with.

    # The allowed scope this key applies to.
    # IE: What APIs this key can be used for.
    scope = models.CharField(max_length=32, default="")


    # Role should never be set through any public method.
    # This determines how much access this key permits it's given scope.
    # Default role should be r-
    # r- Limited Read --- limted by client
    # r+ Read All --- read every client
    # rw- Limited RW --- limited by client
    # rw+ Read/Write All --- read write everything
    role = models.CharField(max_length=32, default="")  # max role for user


    # TODO
    last_attempt_auth = models.DateTimeField(null=True)
    fail_count = models.IntegerField(default=0)
    banned = models.BooleanField(default=False)


class AuthHistory(models.Model):
    key = models.ForeignKey(AuthKey, on_delete=models.DO_NOTHING)# the key attempting to auth
    date = models.DateTimeField()# Set to auto set
    result = models.CharField(max_length=16) # whether it was allowed or not
    note =  models.CharField(max_length=128) # free text set from auth functions