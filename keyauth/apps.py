from django.apps import AppConfig


class KeyauthConfig(AppConfig):
    name = 'keyauth'
