from django.contrib import admin
from keyauth.models import *
import os

# only register models during development
if os.getenv("DEVELOPMENT", "0") == "1":
    admin.site.register(AuthKey)
    admin.site.register(AuthHistory)
