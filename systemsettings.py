from cvedb.models import *

class SystemSettings():
    def cve_recent_url(self):
        return "https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-modified.json.zip"

    def verbosity(self):
        #return Settings.objects.all()[0].verbosity
        return 3


settings = SystemSettings()




# logs/prints based on verbose level
# 1 - 10, 10 being the most verbose
# 1 - prints major event completions
def plog(message, sev=10):
    if sev <= settings.verbosity():
        print(message)