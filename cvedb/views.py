from django.shortcuts import render
from cvedb.models import CVEStatus
from django.http import HttpResponse
from cvedb.cve  import start_recent_load
from cvedb.search import get_cves_from_product
from threading import Thread
import os
from keyauth.views import auth_key

# Create your views here.


def status(request):
    state = CVEStatus.objects.all()[0]

    return render(request,"api1_status.json",{ 'state': state.state})


def api1_get_cves_from_product(request):
    if os.getenv("DEVELOPMENT","0") != "1":
        api_id = request.POST.get("id","not_set")
        key = request.POST.get("key","not_set")
        auth, message = auth_key(user=api_id, key=key)
        print("Authenticating: "+api_id+" : "+message)
        if auth == False:
            return "ERROR" # TODO - perform legit access denied response.


    name = request.GET.get("name","excel")
    exact = request.GET.get("exact","false")

    if exact == "false":
        exact = False
    else:
        exact = True
    cves = get_cves_from_product(name, exact=exact)
    return render(request, "api1_get_cves_from_products.json", { 'total': cves.count(), 'cves':cves })


def api1_start_latest_load(request):
    if os.getenv("DEVELOPMENT","0") != "1":
        api_id = request.POST.get("id","not_set")
        key = request.POST.get("key","not_set")
        auth, message = auth_key(user=api_id, key=key)
        print("Authenticating: "+api_id+" : "+message)
        if auth == False:
            return "ERROR" # TODO - perform legit access denied response.

    try:
        thread = Thread(target=start_recent_load)
        thread.start()
        return HttpResponse('{"Started":"Yes"}')
    except:
        return HttpResponse('{"Started":"No"}')

