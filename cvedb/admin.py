from django.contrib import admin
from cvedb import models
# Register your models here.
admin.site.register(models.Product)
admin.site.register(models.CVE)
admin.site.register(models.CVEStatus)
admin.site.register(models.Translate)