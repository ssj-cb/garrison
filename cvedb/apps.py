from django.apps import AppConfig


class CvedbConfig(AppConfig):
    name = 'cvedb'
