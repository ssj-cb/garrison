# Generated by Django 2.0 on 2018-08-22 22:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cvedb', '0006_translate'),
    ]

    operations = [
        migrations.AddField(
            model_name='translate',
            name='target',
            field=models.CharField(choices=[('vendor', 'Vendor'), ('product', 'Product'), ('version', 'Version')], default='product', max_length=10),
        ),
    ]
