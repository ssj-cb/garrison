from django.db import models

# Create your models here.


# Basic security model.
# Mostly implemented at application level.
# For most auth, requrie account key, username, and user key.


class Settings(models.Model):
    cve_base_url = models.CharField(max_length=255)
    verbosity = models.IntegerField(default=5)



# The current state of the CVE portion of
class CVEStatus(models.Model):

    motd = models.CharField(max_length=32) # Free text used for basic testing.


    # Downloading - Scanning and updating changes
    # Processing - Organizing and processing
    # Stable - Safe to read/write
    state = models.CharField(max_length=32)# Text field describing current status.

class Product(models.Model):
    name = models.CharField(max_length=64) # the product name. Used as key in many searches.
    vendor = models.CharField(max_length=64)
    version = models.CharField(max_length=64)
    cves = models.ManyToManyField('CVE')

    def __str__(self):
        return self.vendor+" | "+self.name+" | "+self.version

# Raw un-alter CVE data
class CVE(models.Model):
    name = models.CharField(max_length=32) # the CVE name. Used as key in many searches.
    description = models.CharField(max_length=1024) #sometimes the only other data provided

    products = models.ManyToManyField(Product)
    # TODO - save other information.

    def __str__(self):
        return self.name+" : "+self.description



TRANSLATE_TYPES = (('regex','Regex'), ('static', 'Static'))
TRANSLATE_TARGETS = (('vendor', 'Vendor'), ('product', 'Product'), ('version', 'Version'))

class Translate(models.Model):
    type = models.CharField(max_length=10, choices=TRANSLATE_TYPES, default='static')
    target = models.CharField(max_length=10, choices=TRANSLATE_TARGETS, default='product')
    match = models.CharField(max_length=32,default="not_set")
    to = models.CharField(max_length=32,default="not_set")

    def __str__(self):
        return self.type+" - "+self.target + ": " +self.match+" = "+self.to
