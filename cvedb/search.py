from cvedb.models import *
import re

def get_cves_from_product(vendor_name="", product_name="", version_name="", exact=True):
    cve = None
    if exact:
        cve = CVE.objects.filter(products__name=product_name, products__vendor=vendor_name, products__version=version_name)
    else:
        cve = CVE.objects.filter(products__name__contains=product_name, products__vendor__contains=vendor_name, products__version__contains=version_name)
    return cve


def product_match(vendor="", product="", version=""):
    vendor = translate_item("vendor", vendor)
    product = translate_item("version", product)
    version = translate_item("version", version)
    cves = get_cves_from_product(vendor, product, version, exact=False)
    if cves.count() > 0:
        return cves
    return None


def translate_item(target="vendor",item=""):
    translation = Translate.objects.filter(target=target) # find a tran that is of this target
    if translation.count() != 0: # if there is a match
        for tranny in translation: # loop every match
            if tranny.type == "static": #if of type static -> staticly translate it
                if item == tranny.match: # if the given item is = match, translate it
                    return tranny.to
            elif tranny.type == "regex":
                if re.match(tranny.match, item) is not None:
                    return tranny.to

    return item # no matches

