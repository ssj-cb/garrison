# File stores funcs/classes related to raw CVE data.
from systemsettings import *
import requests
import zipfile
import json
import io

# Internal
# Download All JSON Data
# Download Latest JSON Data
# Parse any given JSOn to classes X
# Search existing DB for class X
# Update and track new/changes to DB
# Report on new/change to DB

class ProductHolder():
    product_name="not_set"
    version_data = None

    def __init__(self):
        self.version_data = []

class VendorHolder():
    vendor_name="not_set"
    product_data = None

    def __init__(self):
        self.product_data = []


# Class that holds CVE data before being loaded into DB
class CVEHolder():
    data_version="not_set"
    id="not_set"
    assigner="not_set"
    vendor_data = None
    description = ""

    def __init__(self):
        self.vendor_data = []



# creates anew product and/or returns an existing one.
def get_product(product, vendor, version):
    product_db = Product.objects.filter(name=product, vendor=vendor, version=version)

    if product_db.count() == 0:  # if doesn't exist, create it
        product_db = Product()
        product_db.name = product
        product_db.version = version
        product_db.vendor = vendor
        product_db.save()
    else:
        product_db = product_db[0]
    return product_db

# the CVEHolder and product_db to associate it with
# TODO return error codes
def associate_cve_product(cve_id, product_db):
    # Query if cve is alrady attached to product

    cve_with_product = CVE.objects.filter(name=cve_id.name,
                                          products__id=product_db.id)  # See if product is already attached
    if cve_with_product.count() == 0:  # Not attached, attach cve to product and product to cve.
        product_db.cves.add(cve_id)
        product_db.save()
        cve_id.products.add(product_db)
        cve_id.save()
        plog("cvedb.cve.save_product: Attempted to attach produce/cve: " + str(product_db) + " | " + cve_id.name,1)
    else:
        print("cvedb.cve.save_product: Product already related. " + str(product_db) + cve_id.name + " db count: " + str(cve_with_product.count()))

def product_save(cve_to_save):
    cve_id = CVE.objects.filter(name=cve_to_save.id)

    if cve_id.count() == 1:
        vendor = "not_set"
        vendor_count = len(cve_to_save.vendor_data)
        cve_id = cve_id[0]
        if vendor_count != 0:
            for vend in cve_to_save.vendor_data:
                plog("cvedb.cve.save_product: vendor: "+str(vend.vendor_name),1)
                vendor = vend.vendor_name.lower()
                for prod in vend.product_data:
                    plog("cvedb.cve.save_product:                     product: " + str(prod.product_name),9)
                    product = prod.product_name.lower()
                    for version in prod.version_data:
                        plog("cvedb.cve.save_product:                                version: " + str(version),9)
                        version = version.lower()
                        # save or get the product database object
                        product_db = get_product(product, vendor, version)
                        # assocaite the cve and product db objects
                        associate_cve_product(cve_id,product_db)
        else:
            print("cvedb.cve.save_product: cve has no vendor data: "+cve_to_save.id)
    else:
        print("cvedb.cve.save_product: CVE Does not exist.")

# Takes a CVEHolder and submits it to the database
# In unfiltered state. Saves CVE Information but not
# Associated products.
def cve_save(cve_to_save):
    cve = CVE.objects.filter(name=cve_to_save.id)

    if cve.count() == 0: # CVE doesn't exist
        c = CVE()
        c.name = cve_to_save.id
        c.description = cve_to_save.description
        c.save()
        return c
    else: # TODO - if CVE does exist. Update It.
        return cve[0]

# Stop can be used for testing so you don't have to DL and process the entire feed
def save_cveholders(all_cves, stop = -1):
    i = 0
    for cve in all_cves:
        i+=1
        if i == stop:
            break
        cve_save(cve)
        product_save(cve)
    state_finish_processing()

# TODO make this legit
def sanitize_cve_description(description):
    return description.replace("\"","'")


def state_start_download():
    state = CVEStatus.objects.all()[0]
    state.state = "Downloading"
    state.save()

def state_finish_download():
        state = CVEStatus.objects.all()[0]
        state.state = "Downloaded"
        state.save()

def state_start_processing():
    state = CVEStatus.objects.all()[0]
    state.state = "Processing"
    state.save()

def state_finish_processing():
    state = CVEStatus.objects.all()[0]
    state.state = "Stable"
    state.save()

def get_system_state():
    state = CVEStatus.objects.all()[0]
    return state.state

# Checks states, returns true if it's okay to proceed.
# Should be called before any CVE processing is started.
#
def start_download_state_check():
    state = get_system_state()
    if state != "Stable":
        return False
    return True

def start_processing_state_check():
    state = get_system_state()
    if state != "Downloaded":
        return False
    return True

# Takes the json object and turns it into an array of CVEHolder objects
def cve_from_json(json_object):
    print(" ===== cve from json ===== ")
    state_start_processing()
    print("Data version: " + json_object["CVE_data_version"])
    all_cves = []
    for c in json_object["CVE_Items"]:
        cve_vers = c["cve"]["data_version"]
        cveid = c["cve"]["CVE_data_meta"]["ID"]
        assigner = c["cve"]["CVE_data_meta"]["ASSIGNER"]
        # TODO Check length of description data array
        description = c["cve"]["description"]["description_data"][0]["value"]
        print(cveid)
        cve = CVEHolder()
        cve.id = cveid
        cve.assigner = assigner
        cve.data_version = cve_vers
        cve.description = sanitize_cve_description(description)

        # Loop for each affected vendor
        for a in c["cve"]["affects"]["vendor"]["vendor_data"]:
            vendor_name = a["vendor_name"]

            vendor = VendorHolder()
            vendor.vendor_name = vendor_name
            #print(vendor.vendor_name)

            for p in a["product"]["product_data"]:
                product_name = p["product_name"]

                product = ProductHolder()
                product.product_name = product_name

                plog(product.product_name)
                for v in p["version"]["version_data"]:
                    version_value = v["version_value"]

                    product.version_data.append(version_value)

                    #print(version_value)
                # Add product to vendor
                vendor.product_data.append(product)
            cve.vendor_data.append(vendor)# Add vendor to cve
        all_cves.append(cve) #append cve to list
    return all_cves

def json_from_file(file):
    print("FILE: "+file)
    json_data = None
    with open(file, "r") as f:
        content = f.read()
        json_data = json.loads(content)
        print("json from file, file timestamp:" + json_data["CVE_data_timestamp"])
    return json_data




# Downloads CVE data from mitre and returns JSon data
# Param
#   url - the uril to the zip file
# Return
#   json data generated with the json library.
def load_mitre_cve(year="2018", recent=False):

    if start_download_state_check() == False:
        plog("load_mitre_cve: Attempted to start download, but system status is "+get_system_state(),2)
        return None

    state_start_download()

    if recent is False:
        base = "http://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-"
        url = base + year +".json.zip"
    else:
        url = settings.cve_recent_url()

    plog(" Downloading zip file.....")
    r = requests.get(url)
    plog("Processing zip.....")
    z = zipfile.ZipFile(io.BytesIO(r.content))
    z.extractall()
    plog(z.namelist())
    plog("Reading file contents.....")
    f = z.open(z.namelist()[0])
    content = f.read()
    content_string = content.decode()

    #type(content_string)
    plog("Parsing JSON....")
    json_data = None
    try:
        json_data = json.loads(content.decode())
    except MemoryError:
        plog("###########################################")
        plog("load_mitre_cve: ERROR: json for year: "+year+" - is to large to process...",1)
        plog("###########################################")

    state_finish_download()

    return json_data



# Starts and saves recently change CVEs
def start_recent_load():
    json = load_mitre_cve(recent=True)
    if json is not None:
        cves = cve_from_json(json)
        save_cveholders(cves)
    else:
        plog("start_recent_load: ERROR: Unable to process CVEs", 1)
        return None