FROM python:3.6

RUN apt-get update && pip install uwsgi

WORKDIR /app

ADD . /app/

RUN pip install -r requirements.txt

ENV DJANGO_ENV=prod
ENV DJANGO_CONTAINER=1

EXPOSE 80

ENTRYPOINT ["python3","/app/manage.py","runserver","0.0.0.0:80"]
