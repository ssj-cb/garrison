from systemsettings import *
from cvedb.cve import *
from cvedb.search import *


def tesstatictload(filepath="c:/checkblue/garrison/static/nvdcve-1.0-2017.json"):
    j = json_from_file(filepath)
    cves = cve_from_json(j)
    save_cveholders(cves)


def testload():
    json = load_mitre_cve(recent=True)
    cves = cve_from_json(json)
    save_cveholders(cves)

def testquery():
    for cve in product_match("microsoft","excel"):
        print("||| "+ str(cve)+" |||")


def test():
    #testload()
    testquery()

test()
